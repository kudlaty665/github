//
//  ViewController.m
//  testApp
//
//  Created by Przemysław Wośko on 24.06.2015.
//  Copyright (c) 2015 pw. All rights reserved.
//

#import "PickUserViewController.h"
#import "GITHubHTTPRequestManager.h"
#import "RepositoriesTableViewController.h"

static NSString * const ReposListScreenSegueID = @"ReposListScreenSegueID";

@interface PickUserViewController ()

// Outlets
@property (weak, nonatomic) IBOutlet UITextField *userTextField;

@property (strong, nonatomic) NSArray *userRepos;
@property (assign, nonatomic) BOOL isChecking;

@end

@implementation PickUserViewController

#pragma mark - Lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.userRepos = @[];
}

#pragma mark - Private
- (void)showErrorWithError:(NSError*)error
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Error occured!" message:error.localizedDescription preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:(UIAlertActionStyleDefault) handler:nil]];
    [self presentViewController:alertController animated:YES completion:nil];
}

#pragma mark - Actions
- (IBAction)checkUserAction:(id)sender
{
    NSString *userName = self.userTextField.text;
    self.isChecking = YES;  // Prevent multiple calls
    
    __weak typeof(self) weakSelf = self;
    [[GITHubHTTPRequestManager sharedManager] getUserReposWithUsername:userName completion:^(BOOL success, NSError *error, NSArray *repositories) {
        if(success)
        {
            weakSelf.userRepos = repositories;
            [weakSelf performSegueWithIdentifier:ReposListScreenSegueID sender:weakSelf];
        }
        else
        {
            [weakSelf showErrorWithError:error];
        }
        weakSelf.isChecking = NO;
    }];
}

- (IBAction)dismissKeyboard:(id)sender
{
    [self.userTextField resignFirstResponder];
}

#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString:ReposListScreenSegueID])
    {
        if([segue.destinationViewController isKindOfClass:[RepositoriesTableViewController class]])
        {
            ((RepositoriesTableViewController*)segue.destinationViewController).repositories = self.userRepos;
        }
    }
}

@end
