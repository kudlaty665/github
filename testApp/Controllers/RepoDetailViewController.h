//
//  RepoDetailViewController.h
//  testApp
//
//  Created by Przemysław Wośko on 24.06.2015.
//  Copyright (c) 2015 pw. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Repository;

@interface RepoDetailViewController : UIViewController

@property (strong, nonatomic) Repository *repository;

@end
