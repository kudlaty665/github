//
//  RepoDetailViewController.m
//  testApp
//
//  Created by Przemysław Wośko on 24.06.2015.
//  Copyright (c) 2015 pw. All rights reserved.
//

#import "RepoDetailViewController.h"
#import "Repository.h"

@interface RepoDetailViewController ()

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *idLabel;
@property (weak, nonatomic) IBOutlet UILabel *urlLabel;
@property (weak, nonatomic) IBOutlet UILabel *stargazersLabel;
@property (weak, nonatomic) IBOutlet UILabel *watchersLabel;
@property (weak, nonatomic) IBOutlet UILabel *forksLabel;

@end

@implementation RepoDetailViewController

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.nameLabel.text = self.repository.repoName;
    self.idLabel.text = [self.repository.repoId stringValue];
    self.urlLabel.text = self.repository.repoUrl;
    self.stargazersLabel.text = [NSString stringWithFormat:@"Stars : %@",[self.repository.stargazersCount stringValue] ];
    self.watchersLabel.text = [NSString stringWithFormat:@"Watchers : %@", [self.repository.watchersCount stringValue]];
    self.forksLabel.text = [NSString stringWithFormat:@"Forks : %@",[self.repository.forksCount stringValue]];
}


@end
