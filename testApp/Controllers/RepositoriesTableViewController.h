//
//  RepositoriesTableViewController.h
//  testApp
//
//  Created by Przemysław Wośko on 24.06.2015.
//  Copyright (c) 2015 pw. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RepositoriesTableViewController : UITableViewController

@property (strong, nonatomic) NSArray *repositories;

@end
