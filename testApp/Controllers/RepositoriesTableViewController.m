//
//  RepositoriesTableViewController.m
//  testApp
//
//  Created by Przemysław Wośko on 24.06.2015.
//  Copyright (c) 2015 pw. All rights reserved.
//

#import "RepositoriesTableViewController.h"
#import "Repository.h"
#import "RepoDetailViewController.h"

static NSString * const ShowDetailSegueID = @"ShowDetail";

@interface RepositoriesTableViewController ()

@property (strong, nonatomic) Repository *repoToShow;

@end

@implementation RepositoriesTableViewController

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.repositories.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    
    Repository *repo = self.repositories[indexPath.row];
    
    cell.textLabel.text = repo.repoName;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.repoToShow = self.repositories[indexPath.row];
    [self performSegueWithIdentifier:ShowDetailSegueID sender:self];
}

#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString:ShowDetailSegueID])
    {
        if([segue.destinationViewController isKindOfClass:[RepoDetailViewController class]])
        {
            ((RepoDetailViewController*)segue.destinationViewController).repository = self.repoToShow;
        }
    }
}

@end
