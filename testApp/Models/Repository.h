//
//  Repository.h
//  testApp
//
//  Created by Przemysław Wośko on 24.06.2015.
//  Copyright (c) 2015 pw. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Repository : NSObject

@property (strong, nonatomic) NSString *repoName;
@property (strong, nonatomic) NSString *repoUrl;
@property (strong, nonatomic) NSNumber *repoId;
@property (strong, nonatomic) NSNumber *stargazersCount;
@property (strong, nonatomic) NSNumber *watchersCount;
@property (strong, nonatomic) NSNumber *forksCount;



+(NSArray*)parseFromJSON:(id)response;

@end
