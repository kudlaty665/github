//
//  Repository.m
//  testApp
//
//  Created by Przemysław Wośko on 24.06.2015.
//  Copyright (c) 2015 pw. All rights reserved.
//

#import "Repository.h"


static NSString * const IdKey = @"id";
static NSString * const NameKey = @"name";
static NSString * const URLKey = @"html_url";
static NSString * const StarsKey = @"stargazers_count";
static NSString * const WatchersKey = @"watchers_count";
static NSString * const ForksKey = @"forks_count";

@implementation Repository

+(NSArray*)parseFromJSON:(id)response {
    
    NSArray *repos = response;
    NSMutableArray *parsedRepos = [NSMutableArray array];
    for (NSDictionary *repo in repos)
    {
        Repository *r = [[Repository alloc] init];
        
        r.repoId = (repo[IdKey])? repo[IdKey] : nil ;
        r.repoName = (repo[NameKey])? repo[NameKey] : nil ;
        r.repoUrl = (repo[URLKey])? repo[URLKey] : nil ;
        r.stargazersCount = (repo[StarsKey])? repo[StarsKey] : nil ;
        r.watchersCount = (repo[WatchersKey])? repo[WatchersKey] : nil ;
        r.forksCount = (repo[ForksKey])? repo[ForksKey] : nil ;
        
        [parsedRepos addObject:r];
    }
    return parsedRepos;
    
}
@end
