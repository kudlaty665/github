//
//  GITHubHTTPRequestManager.h
//  testApp
//
//  Created by Przemysław Wośko on 24.06.2015.
//  Copyright (c) 2015 pw. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GITHubHTTPRequestManager : NSObject

// Returns shared instance of manager
+(instancetype)sharedManager;

// Performs check for user repos with completion block, than will call on main thread
// For that kind of small calls, NSURLSession is enough, no need to use AFNetworking
// or no need to use NSURLSession
- (void)getUserReposWithUsername:(NSString*)username completion:(void(^)(BOOL success, NSError *error, NSArray *repositories))completion;

@end
