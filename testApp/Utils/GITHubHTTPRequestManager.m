//
//  GITHubHTTPRequestManager.m
//  testApp
//
//  Created by Przemysław Wośko on 24.06.2015.
//  Copyright (c) 2015 pw. All rights reserved.
//

#import "GITHubHTTPRequestManager.h"
#import "Repository.h"

static NSString * const GITHubAPIBaseURL = @"https://api.github.com";
static NSString * const UserReposRouteFormat = @"%@/users/%@/repos";




@implementation GITHubHTTPRequestManager

+ (instancetype)sharedManager
{
    static dispatch_once_t once;
    static id sharedInstance;
    dispatch_once(&once, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

- (void)getUserReposWithUsername:(NSString*)username completion:(void(^)(BOOL success, NSError *error, NSArray *repositories))completion
{
    NSString *urlString = [NSString stringWithFormat:UserReposRouteFormat,GITHubAPIBaseURL,username];
    NSURL *url = [NSURL URLWithString:urlString];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        if(connectionError)
        {
            if(completion)
            {
                // Assure completion will run on main thread
                dispatch_async(dispatch_get_main_queue(), ^{
                    completion(NO,connectionError,nil);
                });
            }
        }
        else
        {
            NSError *serialisationError = nil;
            id responseObject = [NSJSONSerialization JSONObjectWithData:data options:0 error:&serialisationError];
            
            if(serialisationError)
            {
                if(completion)
                {
                    // Assure completion will run on main thread
                    dispatch_async(dispatch_get_main_queue(), ^{
                        completion(NO,serialisationError,nil);
                    });
                }
            }
            else
            {
                // Should be an array
                if([responseObject isKindOfClass:[NSArray class]])
                {
                    
                    NSArray* parsedRepos = [Repository parseFromJSON:responseObject];
                    
                    if(completion)
                    {
                        // Assure completion will run on main thread
                        dispatch_async(dispatch_get_main_queue(), ^{
                            completion(YES,nil,parsedRepos);
                        });
                    }
                }
                else
                {
                    if (completion)
                    {
                        // Assure completion will run on main thread
                        dispatch_async(dispatch_get_main_queue(), ^{
                            completion(NO, [NSError errorWithDomain:@"No users found!" code:404 userInfo:nil],nil);
                        });
                    }
                }
            }
        }
        
    }];
}

@end
