//
//  XCTestCase+ExternalDataUtils.h
//  testApp
//
//  Created by Przemysław Wośko on 6/25/15.
//  Copyright (c) 2015 pw. All rights reserved.
//

#import <XCTest/XCTest.h>

@interface XCTestCase (ExternalDataUtils)
- (id)JSONFromFile:(NSString*)filename;
@end
