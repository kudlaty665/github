//
//  XCTestCase+ExternalDataUtils.m
//  testApp
//
//  Created by Przemysław Wośko on 6/25/15.
//  Copyright (c) 2015 pw. All rights reserved.
//

#import "XCTestCase+ExternalDataUtils.h"

@implementation XCTestCase (ExternalDataUtils)
- (id)JSONFromFile:(NSString*)filename
{
    NSBundle *testBundle = [NSBundle bundleForClass:[self class]];
    NSString *testBundlePath = [testBundle pathForResource:filename ofType:@"json"];
    NSData *myData = [NSData dataWithContentsOfFile:testBundlePath];
    NSError *error = nil;
    id serializedObject = [NSJSONSerialization JSONObjectWithData:myData options: NSJSONReadingMutableContainers error: &error];
    return serializedObject;
}
@end
